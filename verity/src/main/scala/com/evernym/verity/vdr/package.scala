package com.evernym.verity

package object vdr {
  type Namespace = String
  type FQSchemaId = String
  type FQCredDefId = String
  type FQDid = String

  type VdrDid = String
  type VdrSchema = String
  type VdrCredDef = String
  type TxnResult = String
  type TxnSpecificParams = String
}
