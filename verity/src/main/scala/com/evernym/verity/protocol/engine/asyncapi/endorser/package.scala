package com.evernym.verity.protocol.engine.asyncapi

package object endorser {
  val VDR_TYPE_INDY = "indy"
  val ENDORSEMENT_RESULT_SUCCESS_CODE = "ENDT_RES_0001"
}
